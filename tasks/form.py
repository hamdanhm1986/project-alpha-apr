from django.forms import ModelForm
from . import models


class TaskForm(ModelForm):
    class Meta:
        model = models.Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        )
