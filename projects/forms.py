from django.forms import ModelForm
from .models import Project


class project_form(ModelForm):
    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )
