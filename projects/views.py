from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import project_form

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projectlist_object": projects,
    }
    return render(request, "projects/list.html", context)


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = project_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = project_form()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
